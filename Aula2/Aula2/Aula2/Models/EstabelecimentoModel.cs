﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Aula2.Models
{
    public class EstabelecimentoModel
    {
        public String Especialidade { get; set; }
        public String Tipo { get; set; }
        public String Bairro { get; set; }
        public String Cidade { get; set; }
        public String Estado { get; set; }
        public float  Valor { get; set; }
        public float Desconto { get; set; }
        public float Percentual { get; set; }
        public float Resultado { get; set; }
    }
}
