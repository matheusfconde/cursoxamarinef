﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Aula2.Models
{
    public class EstabelecimentoGeralModel
    {
        public EstabelecimentoGeralModel()
        {
            this.Propostas = new List<EstabelecimentoModel>();
        }
        
        [JsonProperty(PropertyName = "pagina_atual")]
        public String Pagina { get; set; }
        [JsonProperty(PropertyName ="total_registros")]
        public int TotalPagina { get; set; }
        [JsonProperty(PropertyName = "Registros")]
        public List<EstabelecimentoModel> Propostas { get; set; }
    }
}
