﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Aula2.Models;
using Newtonsoft.Json;

namespace Aula2.ServicosWeb
{
    public class Consumo
    {
        public string BaseUrl = @"https://redemorse.com.br/sistema/api/saude/rede.php?pagina=0&tipo=&bairro=&cidade=&estado=&esp=";
      
        //Permite receber o objeto "genericamente"
        private List<KeyValuePair<string, string>> parametros = new List<KeyValuePair<string, string>>();

        //Http Client lib de consumo
        public async Task<string> ConsumoApi()
        {
            HttpClient client = new HttpClient();
            //converte em identificadoro de recurso
            client.BaseAddress = new Uri(this.BaseUrl);
            var web = await client.GetAsync(BaseUrl);  //await, aguarde o resultado antes de ir para linha de baixo.
            var resultado = await web.Content.ReadAsStringAsync();
            var serelializado = JsonConvert.DeserializeObject<EstabelecimentoGeralModel>(resultado);

            return resultado;
        }
    }
}
